package com.example

object Hello {
  def main(args: Array[String]): Unit = {
    println("Hello, world!")
    val modulesImplementations = List(new ExampleImplementationA, new ExampleImplementationB)
    modulesImplementations.foreach(impl => impl.print())
  }
}
