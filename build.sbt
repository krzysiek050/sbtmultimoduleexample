import sbt._

name := """multimodule_sbt"""

version := "1.0"

scalaVersion := "2.11.7"

// Change this to another test framework if you prefer
libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.4" % "test"


lazy val common = project.in(file("./common"))
lazy val module_a = project.in(file("./module_a")).aggregate(common).dependsOn(common)
lazy val module_b = project.in(file("./module_b")).aggregate(common).dependsOn(common)
lazy val root = project.in(file(".")).aggregate(module_a, module_b).dependsOn(module_a, module_b)


// Uncomment to use Akka
//libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.3.11"

